package com.test.services;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainProg {
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Connection con = null;
		Statement stmt = null;
		
	
//		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2", "root", "password");
		stmt = con.createStatement();
		
		
		
		//QUERY 1
		/*
		 * Q1. Using JDBC, execute a SQL query to fetch “FIRST_NAME, LAST_NAME” from the Worker
		 * table using the alias name as “FULL_NAME” results in ``UPPER CASE.
		 */
		
		System.out.println("Executing first query...");
		System.out.println();
		
		String query1 = "select FIRST_NAME, POSITION('a' IN FIRST_NAME) AS POSITION_OF_a from Worker";
		
		System.out.println(query1);
		
		ResultSet rs = stmt.executeQuery(query1);
		
		System.out.println("FULL NAMES");
		while(rs.next()) {
			System.out.println(rs.getString(1));
		}
		
		//QUERY 2
		/*
		* Using JDBC, Execute a SQL query to fetch unique values of DEPARTMENT from Worker-
	    * Table.
		*/
		System.out.println();
		System.out.println();
		System.out.println("Executing second query...");
		System.out.println();
				
		String query2 = "SELECT DISTINCT DEPARTMENT FROM Worker";
				
		System.out.println(query2);
				
		ResultSet rs2 = stmt.executeQuery(query2);
				
		System.out.println("UNIQUE DEPARTMENTS IN WORKER TABLE");
		while(rs2.next()) {
			System.out.println(rs2.getString(1));
		}
				
		//QUERY 3
		/*
		* Using JDBC, Execute a SQL query to find the position of the alphabet (‘a’) in the first
		* name column ‘Amitabh’ from the Worker-Table.
		*/	
		System.out.println();
		System.out.println();
		System.out.println("Executing third query...");
		System.out.println();
				
		String query3 = "select FIRST_NAME, POSITION('a' IN FIRST_NAME) from Worker";
				
		System.out.println(query3);
				
		ResultSet rs3 = stmt.executeQuery(query3);
				
		System.out.println("FIRST NAME\tINDEX OF A");
		while(rs3.next()) {
			System.out.println(rs3.getString(1)+"\t"+rs3.getInt(2));
		}
				
		con.close();
		
	}
}
